const input = require('readline-sync');
//figure out if the player is entered greater than 100 or less than one - done
//keep track of the number of guesses, when the game is finished tell the user the number of guesses -done
class Game{
    constructor(){
        this.sPrompt = "I am thinking of a number between 1 and 100. Can you guess it? ";
        this.nComputer=Math.ceil(Math.random()*100);
        this.bDone = false;
    }
    prompt(){
        return this.sPrompt;
    }
    takeATurn(sInput){
        let aResponses = [];
        if(isNaN(sInput)){
            aResponses.push("Please enter a number");
        } else if(sInput<1 || sInput>100){
            aResponses.push("Not even close");
        } else if(sInput>this.nComputer) {
            aResponses.push("Too high");
        } else if(sInput<this.nComputer){
            aResponses.push("Too low");
        } else if(sInput==this.nComputer){
            aResponses.push("That's right!");
            this.bDone=true;
            aResponses.push("You took "+ nGuess +" tries to guess the right number");
        }
        return aResponses;
    }
    isDone(){
        return this.bDone;
    }
}

const oGame = new Game();
let nGuess=0;
while(!oGame.isDone()){
    const sInput = input.question(oGame.prompt());
    console.log(oGame.takeATurn(sInput));
    nGuess++;
}